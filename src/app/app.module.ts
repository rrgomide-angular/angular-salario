import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TempoRealComponent } from './tempo-real/tempo-real.component';
import { CalculoReversoComponent } from './calculo-reverso/calculo-reverso.component';
import { LabeledInputComponent } from './labeled-input/labeled-input.component';

@NgModule({
  declarations: [
    AppComponent,
    TempoRealComponent,
    CalculoReversoComponent,
    LabeledInputComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
