import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-calculo-reverso',
  templateUrl: './calculo-reverso.component.html',
  styleUrls: ['./calculo-reverso.component.css']
})
export class CalculoReversoComponent implements OnInit {
  salarioLiquidoDesejado = 5000;

  @Input() calculandoDados = false;
  @Output() buttonClicked = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  changeSalarioLiquido(newSalarioLiquido) {
    this.salarioLiquidoDesejado = newSalarioLiquido;
  }

  calcularBrutoFromLiquido(salarioLiquidoDesejado) {
    this.buttonClicked.emit(salarioLiquidoDesejado);
  }
}
