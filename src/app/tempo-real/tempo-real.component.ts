import { Salario } from './../classes/Salario';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-tempo-real',
  templateUrl: './tempo-real.component.html',
  styleUrls: ['./tempo-real.component.css']
})
export class TempoRealComponent implements OnInit {
  @Input() salario: Salario;
  @Input() calculandoDados = false;
  @Output() valueChanged = new EventEmitter();

  idInputSalarioBruto = 'inputSalarioBruto';

  constructor() {}

  ngOnInit(): void {
    /**
     * Forçando o autofocus no primeiro
     * input da tela
     */
    const firstInput = document.querySelector('input');
    firstInput.focus();
  }

  changeValue(newSalario) {
    this.valueChanged.emit(newSalario);
  }
}
